# awesome-supply-chain-security

WORK IN PROGRESS

- [owasp-dep-scan/dep-scan](https://github.com/owasp-dep-scan/dep-scan): OWASP dep-scan is a next-generation security and risk audit tool based on known vulnerabilities, advisories, and license limitations for project dependencies. Both local repositories and container images are supported as the input, and the tool is ideal for integration.
- [govulncheck]https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck)
- [bureado/awesome-software-supply-chain-security](https://github.com/bureado/awesome-software-supply-chain-security): A compilation of resources in the software supply chain security domain, with emphasis on open source
- [jeremylong/DependencyCheck](https://github.com/jeremylong/DependencyCheck): 
- OWASP dependency-check is a software composition analysis utility that detects publicly disclosed vulnerabilities in application dependencies.
- [aquasecurity/chain-bench](https://github.com/aquasecurity/chain-bench): An open-source tool for auditing your software supply chain stack for security compliance based on a new CIS Software Supply Chain benchmark.
- [fossas/fossa-cli](https://github.com/fossas/fossa-cli): Fast, portable and reliable dependency analysis for any codebase. Supports license & vulnerability scanning for large monoliths. Language-agnostic; integrates with 20+ build systems.
- [diffoscope](https://diffoscope.org/): in-depth comparison of files, archives, and directories
- https://deps.dev/: Open Source Insights
- [fossas/fossa-cli](https://github.com/fossas/fossa-cli): Fast, portable and reliable dependency analysis for any codebase. Supports license & vulnerability scanning for large monoliths. Language-agnostic; integrates with 20+ build systems.
- [ossf/package-analysis](https://github.com/ossf/package-analysis): Open Source Package Analysis
- [SpectralOps/preflight](https://github.com/spectralops/preflight): preflight helps you verify scripts and executables to mitigate chain of supply attacks such as the recent Codecov hack.
- [guacsec/guac](https://github.com/guacsec/guac): GUAC aggregates software security metadata into a high fidelity graph database.
- [nexB/scancode-toolkit](https://github.com/nexB/scancode-toolkit): ScanCode detects licenses, copyrights, dependencies by "scanning code" ... to discover and inventory open source and third-party packages used in your code. Sponsored by NLnet project thers generous sponsors(https://nlnet.nl/project/vulnerabilitydatabase, the Google Summer of Code, Azure credits, nexB and )!
- [trailofbits/it-depends](https://github.com/trailofbits/it-depends): A tool to automatically build a dependency graph and Software Bill of Materials (SBOM) for packages and arbitrary source code repositories.
- [DependencyTrack/dependency-track](https://github.com/DependencyTrack/dependency-track): Dependency-Track is an intelligent Component Analysis platform that allows organizations to identify and reduce risk in the software supply chain.
- [oss-review-toolkit/ort](https://github.com/oss-review-toolkit/ort): A suite of tools to automate software compliance checks.
- [anchore/syft](https://github.com/anchore/syft): CLI tool and library for generating a Software Bill of Materials from container images and filesystems
- [tern-tools/tern](https://github.com/tern-tools/tern): Tern is a software composition analysis tool and Python library that generates a Software Bill of Materials for container images and Dockerfiles. The SBOM that Tern generates will give you a layer-by-layer view of what's inside your container in a variety of formats including human-readable, JSON, HTML, SPDX and more.
- [opensbom-generator/spdx-sbom-generator](https://github.com/opensbom-generator/spdx-sbom-generator): Support CI generation of SBOMs via golang tooling.
- [microsoft/sbom-tool](https://github.com/microsoft/sbom-tool): The SBOM tool is a highly scalable and enterprise ready tool to create SPDX 2.2 compatible SBOMs for any variety of artifacts.
- [aquasecurity/chain-bench](https://github.com/aquasecurity/chain-bench): An open-source tool for auditing your software supply chain stack for security compliance based on a new CIS Software Supply Chain benchmark.
- [chainguard-dev/ssc-reading-list](https://github.com/chainguard-dev/ssc-reading-list): A reading list for software supply-chain security.
- [snyk-labs/snync](https://github.com/snyk-labs/snync): Mitigate security concerns of Dependency Confusion supply chain security risks
